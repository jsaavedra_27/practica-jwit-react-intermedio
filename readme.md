# React Intermedio

## Contenido
### Video 1
1. Creando componentes con react + goober
2. Reaccionar a un componente goober
3. Reutilizando los props de goober desde el container
4. Editar el estilo de una aleatorio usando los classname
5. Creando el componente aleatorio

### Video 2
1. Uso de variables css
2. Logica de selected para el mask
3. Separando la logica es un custom hook

### Video 3
1. Instalar la dependencia EmitterEvent3
2. Usando EmiiterEvent para reemplazar los callback entre el aleatorio y el custom hook
3. Ejercicio 1: cambiar el id  con un boton
4. Ejercicio 2: escuchar los cambios de data desde un componente

### Video 4
1. Correccion del video 3
2. configurando el store
3. Creando el slice options  y conectarlo al store
4. Configuracion del store para ser usado desde los componentes
5. Usar useAppDispath y useSelector en el component
6. Pasar la logica del componente al hook useSelect

### Video 5
1. Trabajando las vista en figma
2. Creando el componente de la vista principal
3. Creando un componente button
4. Creando un componente miniCard

### Video 6
1. Ajuste de la vista principal
2. Creando el vista detalle
3. Nuevas propiedades al componente Button
3. Usar una transicion para el visible de la vista detalle