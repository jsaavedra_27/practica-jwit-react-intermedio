import React, { useEffect, useState } from 'react'
import { styled } from 'goober'
import Vista from './component'
import Detalle from './detalle'

const Container = styled('div')`
  height: 100vh;
  position: relative;

  --font-family: 'Gill Sans', 'Gill Sans MT', Calibri, 'Trebuchet MS', sans-serif;
  --card-background:#2E2F31;
  --card-border: 1px solid #2E2F31;
  --card-selected: #2c45d356;

  * {
    font-family: var(--font-family);
    box-sizing: border-box;
  }
`

const App = (): JSX.Element => {

  const [visible, setVisible] = useState<boolean>(false)

  const handleClick = () => setVisible(true)

  const handleBack = () => setVisible(false)

  return (
    <Container >

      <Vista image='/2.jpg' onClick={handleClick}/>

      <Detalle 
        image='/1.jpg' 
        price={400} 
        visible={visible}
        title="Valles del Sol"
        description='Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam'
        onClickBack={handleBack}
      /> 

    </Container>
  )
}

export default App