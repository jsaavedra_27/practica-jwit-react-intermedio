import React, { CSSProperties } from 'react'
import { styled } from 'goober'

const Container = styled('button')`   
  border-radius: 10px;
  background-color: #D73E3E;
  border: 1px solid #D73E3E;
  color: #fff;  
  width: 100%;
  height: 40px;
  font-size: 18px;
`

export interface params {
  label: string;
  className: string;
  onClick?: () => void;
}

/**
 * 
 * @param params 
 * @returns JSX.Element
 */
const App = (params: params): JSX.Element => {

  return (
    <Container className={params.className} onClick={params.onClick}>
      {params.label}
    </Container>
  )
}

App.defaultProps = {
  className: 'button'
}

export default App