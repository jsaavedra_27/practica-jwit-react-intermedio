import React from "react";
import { styled } from "goober";
import Button from './button'
import MiniCard from './miniCard'

const Container = styled("div")<{ image: string }>`
  width: 100vw;
  height: 100vh;
  
  .header {
    width: 100%;
    height: 36vh;
    position: relative;
    background-image: url(${props => props.image});
    background-size: cover;
    background-position: center;

    .cover {
      width: 100%;
      position: absolute;
      left:0px;
      bottom: 10px;
      display: flex;
      align-items: center;
      justify-content: space-around;
    }

    .star{
      margin: 0;
      color: white;

      .icon {
        color: #E9BD6B;
      }
    }

  }

  .title {
    color: #fff;
    margin: 0;
    font-size: 38px;
    width: 70%;
  }

  .mask {
    position: absolute;
    inset: 0;
    background-color: #16171d47;
  }

  .body {
    height: 64vh;
    padding: 10px;
    display: flex;
    flex-direction: column;
    gap: 10px;
    background-color:#16171ddd;
    overflow: hidden;
  }

  .description {
    color: #99969C;
    margin: 0;
  }

  .swipe{ 
    display: flex;
    gap: 15px;
    align-items: center;
    
  }

  .footer {
    display: flex;
    gap: 20px;
  }
`;

export interface params {
  image: string;
  onClick?: () => void;
}

/**
 *
 * @param params
 * @returns JSX.Element
 */
const App = (params: params): JSX.Element => {
  return (
    <Container image={params.image}>

      <div className="header">

        <div className="mask" />

        <div className="cover">

          <h2 className="title">Costa Coffe</h2>

          <p className="star"> <span className="icon">&#9733;</span> 4.2</p>

        </div>

      </div>

      <div className="body">

        <p className="description">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam</p>

        <div className="footer">

          <p className="description">4 personas</p>
          
          <p className="description">Envios a domicilio</p>

        </div>

        <Button label="Solicitar pedido"/>

        <div className="swipe">

          <MiniCard image="/3.jpg" title="Milanesa napolitana" description="15% off"/>

          <MiniCard image="/4.jpg" title="Ensalada dietetica"  description="15% off" onClick={params.onClick}/>

        </div>

      </div>
      
    </Container>
  );
};

export default App;
