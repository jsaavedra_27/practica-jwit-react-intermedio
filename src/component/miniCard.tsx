import React from "react";
import { styled } from "goober";


const Container = styled("div")<{ image: string }>`
  height: 180px;
  width: 200px;
  min-width: 200px;
  background-color: red;
  border-radius: 20px;
  position: relative;
  background-image: url(${(props) => props.image});
  background-position: center;
  background-size: cover;
  background-repeat: no-repeat;

  .mask {
    position: absolute;
    background-color: #16171de2;
    top: unset;
    bottom: 0;
    padding: 8px;
    display: flex;
    flex-direction: column;
    gap: 5px;
  }

  .title {
    font-size: 14px;
    line-height: 15px;
    min-height: 20px;
    width: 100%;
  }

  .description {
    font-size: 11px;
  }
`;

export interface params {
  image: string;
  title: string;
  description?: string;
  onClick?: () => void;
}

/**
 *
 * @param params
 * @returns JSX.Element
 */
const App = (params: params): JSX.Element => {
  return (
    <Container image={params.image} onClick={params.onClick}>

      <div className="mask" >
      
        <h2 className="title">{params.title} </h2>

        <p className="description"> {params.description}</p>

      </div>

    </Container>
  );
};

export default App;
