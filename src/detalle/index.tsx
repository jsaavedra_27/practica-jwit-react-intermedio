import React, { useState } from "react";
import { styled } from "goober";
import Button from '../component/button'

const Container = styled("div")<{ image: string, visible: boolean }>`
  position: absolute;
  inset: 0;
  z-index: 100;
  width: 100vw;
  height: 100vh;
  background-image: url(${props => props.image});
  background-size: cover;
  background-position: center;
  display: flex;
  flex-direction: column;
  justify-content: flex-end;
  transform: translateY(${props => props.visible ? '0': '100%'});
  transition: transform .4s ;

  .card {
    background-color: #16171dd1;
    border-radius: 20px 20px 0px 0px;
    padding: 20px;
    backdrop-filter: blur(4px);
    display: flex;
    flex-direction: column;
    gap: 15px;
  }

  .title {
    color: #fff;
    margin: 0;
    font-size: 38px;
  }

  .description {
    color: #99969C;
    margin: 0;
  }

  .row {
    display: flex;
    align-items: center;
    justify-content: space-between;
    gap: 15px;
  }

  .button {
    width: 65%;
  }

  .btn-back{
    width: 32%;
    background-color: #fff; 
    border-color: #fff;
    color: #16171d;
  }

  .btn-operator {
    width: 40px;
    background-color: #16171d; 
    border-color: #16171d;
    color: #fff;
    font-size: 20px;
  }

  .counter {
    color: #fff;
    text-align: center;
    font-size: 20px;
  }

  .price {
    color: #fff;
    font-size: 24px;
    margin: 0;
  }
`;

export interface params {
  image: string;
  price: number;
  visible: boolean;
  title: string;
  description?: string;
  onClickBack?: () => void;
}

/**
 *
 * @param params
 * @returns JSX.Element
 */
const App = (params: params): JSX.Element => {

  const [counter, setCounter] = useState<number>(0)

  const handleAdd = () => setCounter(counter + params.price)

  const handleSub = () => counter - params.price >= 0 && setCounter(counter - params.price)

  return (
    <Container image={params.image} visible={params.visible}>
      
      <div className="card">
        <h2 className="title">{params.title}</h2>
        
        <p className="description">{params.description}</p>

        <div className="row">
          <div className="row operator">
            <Button className="btn-operator" label="-" onClick={handleSub}/>

            <div className="counter">{counter / params.price}</div>

            <Button className="btn-operator" label="+" onClick={handleAdd}/>
          </div>

          <p className="price">$ {counter}</p>
        </div>

        <div className="row">
          <Button label="Pagar"/>

          <Button className="btn-back" label="Volver" onClick={params.onClickBack}/>
        </div>

      </div>

    </Container>
  );
};

export default App;
